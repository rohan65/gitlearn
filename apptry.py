from __future__ import division, print_function
# coding=utf-8
import sys
import os
import glob
import re
#import numpy as np
import cv2
# Keras

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer


app = Flask(__name__)


MODEL_PATH = r'/home/ubuntu/tm/model.h5'

model = load_model(MODEL_PATH)

print('Model loaded. Check http://127.0.0.1:5000/')


def model_predict(img_path):
    image = cv2.imread(img_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224,224))
    image = np.array(image)
    image = image.reshape(-1,224,224,3)
    preds = model.predict(image)
    return np.round(preds[0][0])


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/predict', methods=['POST'])
def upload():
    if request.method == 'POST':  
        f = request.files['file'] 
        basepath = os.path.dirname(__file__)
        file_path = os.path.join(
            basepath, 'uploads', secure_filename(f.filename))
        f.save(file_path) 
        prediction = model_predict(file_path)
        a='Male'
        if(prediction==1):
            a='Female'
        
    return render_template('index.html', prediction_text='person is {}'.format(a))


if __name__ == '__main__':
    app.run(debug=True)
